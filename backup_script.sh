#!/bin/bash

# Enable maintenance mode
ssh root@nextcloud 'sudo -u www-data php /var/www/html/nextcloud/occ maintenance:mode --on'

# Perform mysql dump of db
ssh root@nextcloud "mysqldump --single-transaction -h localhost -u root --password=root nextcloud" > /data/backup/nextcloud_bdd.bak

# Transfer backup to backup serv
rsync -Aavx -e "ssh" root@nextcloud:/var/www/html/nextcloud/ /data/backup/nextcloud_data/

# Create snapshot of backup
zfs snapshot data/backup@nextcloud_`date +"%Y-%m-%d"`

# Delete snapshot older than 30 days
limite="data/backup@nextcloud_`date --date='-30 day' +"%Y-%m-%d"`"
for snap in `zfs list -H -t snapshot -o name` ; do
	if [[ $snap < $limite ]]; then
		zfs destroy $snap
	fi
done

# Disable maintenance mode
ssh root@nextcloud 'sudo -u www-data php /var/www/html/nextcloud/occ maintenance:mode --off'

echo "########## BACKUP COMPLETE ##########"