# nextcloud_app_backup_tp_sauvegarde

Go to /etc/ssh on nextcloud app server
Change file sshd_config
Uncomment or add lign :
	PermitRootLogin yes
Restart ssh service :
$ service ssh restart

If .sh file are not runnable with ./my_script.sh do :
$ chmod +x ./my_script.sh 


In first, launch setup_connexion_script.sh
- Enter yes if needed
- Enter password: root if needed

For backup nextcloud app and db, use script backup_script.sh

For restore nextcloud app and db, use script backup_restore.sh
- Follow instructions

We can automate backup task with crontab.
- Type $ crontab -e for create a crontask
- The following syntax is used : 

		- m h dom mon dow user cmd
		
		(where : 
		m			minutes				0-59
		h			hour				0-23
		dom			day of month		1-31
		mon			month				1-12
		dow			day of week			0-6
		user		user				valid user
		command		command				valid cmd)

		Exemple : 00 23 * * * root /scriptpath/backup_save.sh
		The script will execute every day at 11:pm