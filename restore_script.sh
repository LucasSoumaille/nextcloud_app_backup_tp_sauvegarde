#!/bin/bash

# Ask for date pattern
echo "Type a date wich you want to use for the restoration : YYYY-MM-DD"
read date

# Enable maintenance mode
ssh root@nextcloud 'sudo -u www-data php /var/www/html/nextcloud/occ maintenance:mode --on'

# Snapshot clone
zfs clone data/backup@nextcloud_$date data/restore

# Restoration of nextcloud app
rsync -Aavx /data/restore/nextcloud_data/ -e "ssh" root@nextcloud:/var/www/html/nextcloud/

# Restoration of nextcloud db
ssh root@nextcloud "mysql -h localhost -u root --password=root nextcloud" < /data/restore/nextcloud_bdd.bak

# Delete snapshot
zfs destroy data/restore

# Disable maintenance mod
ssh root@nextcloud "sudo -u www-data php /var/www/html/nextcloud/occ maintenance:mode --off"

echo "########## RESTORATION COMPLETE ##########"