#! /bin/bash

# Generate key
ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa

# Send key then press yes and root password
ssh-copy-id -i ~/.ssh/id_rsa.pub root@nextcloud